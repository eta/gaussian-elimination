(in-package :cl-user)

(defmacro with-each-row ((row-variable matrix) &body body)
  "For each row in MATRIX, bind ROW-VARIABLE to its index and evaluate BODY."
  `(loop
     for ,row-variable from 0 below (array-dimension ,matrix 0)
     do (progn ,@body)))

(defmacro with-each-col ((col-variable matrix) &body body)
  "For each column in MATRIX, bind COLUMN-VARIABLE to its index and evaluate BODY."
  `(loop
     for ,col-variable from 0 below (array-dimension ,matrix 1)
     do (progn ,@body)))

(defun interchange-rows (matrix a b)
  "Interchanges the rows indexed by A and B of MATRIX."
  (with-each-col (i matrix)
    (rotatef (aref matrix a i) (aref matrix b i))))

(defun scale-row (matrix row scalar)
  "Multiply ROW of MATRIX by SCALAR in-place."
  (with-each-col (i matrix)
    (setf (aref matrix row i) (* scalar
                                 (aref matrix row i)))))

(defun subtract-rows (matrix a b &optional (multiple 1))
  "Subtract MULTIPLE times row B from row A of MATRIX."
  (with-each-col (i matrix)
    (setf (aref matrix a i) (- (aref matrix a i)
                               (* multiple (aref matrix b i))))))

(defun find-pivot (matrix)
  "Find a pivot in MATRIX. Returns (ROW COLUMN) or NIL if no pivot can be found."
  (with-each-col (col matrix)
    (with-each-row (row matrix)
      (unless (eql (aref matrix row col) 0)
        (return-from find-pivot (list row col))))))

(defun create-zeroes-under-pivot (matrix pivot-col)
  "Subtract appropriate multiples of the first row of MATRIX from all other rows to obtain zeroes under PIVOT-COL."
  (loop
    for row from 1 below (array-dimension matrix 0)
    unless (eql (aref matrix row pivot-col) 0)
      do (subtract-rows matrix row 0 (/ (aref matrix row pivot-col)
                                        (aref matrix 0 pivot-col)))))

(defun create-submatrix (matrix start-row start-col)
  "Create a submatrix of MATRIX, consisting of all rows including and after START-ROW and all columns including and after START-COL."
  (let* ((orig-rows (array-dimension matrix 0))
         (orig-cols (array-dimension matrix 1))
         (new-cols (- orig-cols start-col))
         (new-rows (- orig-rows start-row))
         (ret (make-array (list new-rows new-cols))))
    (loop
      for row from 0 below new-rows
      do (loop
           for col from 0 below new-cols
           do (setf (aref ret row col)
                    (aref matrix (+ row start-row) (+ col start-col)))))
    ret))

(defun copy-back-from-submatrix (matrix submatrix start-row start-col)
  "Copy the elements in SUBMATRIX (created by passing the given START-ROW and START-COL) back into MATRIX."
  (with-each-row (row submatrix)
    (with-each-col (col submatrix)
      (setf (aref matrix (+ row start-row) (+ col start-col))
            (aref submatrix row col)))))

(defun copy-matrix (matrix)
  "Copy MATRIX."
  (let* ((dimensions (array-dimensions matrix))
         (ret (make-array dimensions
                          :element-type (array-element-type matrix))))
    (dotimes (i (array-total-size matrix))
      (setf (row-major-aref ret i)
            (row-major-aref matrix i)))
    ret))

(defun matrix-rank (matrix)
  "Determine the rank of MATRIX."
  (let ((matrix (reduced-row-echelon-form (gaussian-elimination (copy-matrix matrix))))
        (nonzero-rows 0))
    (with-each-row (row matrix)
      (with-each-col (col matrix)
        (unless (eql (aref matrix row col) 0)
          (incf nonzero-rows)
          (return))))
    nonzero-rows))

(defun reduced-row-echelon-form (matrix)
  "Put MATRIX, a matrix in row-echelon form, into reduced row echelon form. Makes a copy of MATRIX first."
  (with-each-row (row matrix)
    (let ((first-nonzero-col
            (with-each-col (col matrix)
              (unless (eql (aref matrix row col) 0)
                (return (aref matrix row col))))))
      (when first-nonzero-col
        (scale-row matrix row (/ 1 first-nonzero-col)))))
  matrix)

(defun gaussian-elimination (matrix)
  "Perform Gaussian elimination on MATRIX."
  (let ((pivot '(0 0))
        (submatrix-params '(0 0))
        (submatrix matrix))
    (loop
      while (setf pivot (find-pivot submatrix))
      do (progn
           (unless (eql (car pivot) 0)
             (interchange-rows submatrix 0 (car pivot)))
           (create-zeroes-under-pivot submatrix (cadr pivot))
           (copy-back-from-submatrix matrix submatrix (car submatrix-params) (cadr submatrix-params))
           (pretty-print-matrix matrix)
           (setf submatrix-params (mapcar #'+ submatrix-params pivot '(1 1)))
           (setf submatrix (create-submatrix matrix (car submatrix-params) (cadr submatrix-params)))))
    matrix))

(defun pretty-print-matrix (matrix &optional (out *standard-output*))
  "Pretty-print MATRIX."
  (let* ((widest-number (loop
                          for i from 0 below (reduce #'* (array-dimensions matrix))
                          maximizing (length (princ-to-string (row-major-aref matrix i)))))
         (len (array-dimension matrix 1))
         (n-spaces (+ (* len widest-number)
                      (* 2 (1- len))))
         (spaces (make-string n-spaces
                              :initial-element #\Space)))
    (format out "┏~A┓~%" spaces)
    (with-each-row (row matrix)
      (format out "┃")
      (with-each-col (col matrix)
        (format out "~vD" widest-number (aref matrix row col))
        (unless (eql col (1- (array-dimension matrix 1)))
          (format out "  ")))
      (format out "┃~%"))
    (format out "┗~A┛~%" spaces))
  matrix)
